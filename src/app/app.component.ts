import { Component } from '@angular/core';
import { UserService } from './user.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  isLogged: Observable<boolean>; 

  constructor(private userService: UserService) {}

  ngOnInit() {
    this.isLogged = this.userService.isLogged; 
  }

  logout() {
    this.userService.logout();
  }
}
