import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  registerForm: FormGroup;

  constructor(private userService: UserService, private formBuilder: FormBuilder) { }

  error;

  ngOnInit() {
    this.regForm();
  }

  regForm() {
    this.registerForm = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', [Validators.required, Validators.minLength(5)]],
      confirmPassword: ['', Validators.required]
    }, {validator: this.checkPasswords })
  }

  checkPasswords(group: FormGroup) {
  let pass = group.controls.password.value;
  let confirmPass = group.controls.confirmPassword.value;

  return pass === confirmPass ? null : { notSame: true }   
  }

  onSubmitForm() {
    let value = this.registerForm.value;
    this.userService.register(value).subscribe(
      data => {
          console.log("POST Request is successful ", data);
      },
      error => {
          console.log("Error", error);
    });
  }

  register(username, password, confirmPassword) {
    if(password === confirmPassword) {
      let user = {"username": username, "password": password};
      this.userService.register(user).subscribe(
        data => {
            console.log("POST Request is successful ", data);
        },
        error => {
            console.log("Error", error);
      });
    }
    else {
      this.error = "password different confirm password";
    }
  }

}
