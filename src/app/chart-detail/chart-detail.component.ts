import { Component, OnInit, Input } from '@angular/core';
import '../../../dependance/Chart.Financial.js'

@Component({
  selector: 'app-chart-detail',
  templateUrl: './chart-detail.component.html',
  styleUrls: ['./chart-detail.component.css']
})
export class ChartDetailComponent implements OnInit {

  @Input() dataChart: any;

  public datasets:Array<any> = [
    {data: [], fractionalDigitsCount: 2}
  ];

  chartOptions = {
    tooltips: {
      position: 'nearest',
      mode: 'index',
      titleFontSize: 12,
      bodyFontSize: 12,
    },
    scales: {
      xAxes: [{
        gridLines: {
          display:false
        }  
      }]
    }
  }

  constructor() { }

  ngOnInit() {
    this.transformData();
  }

  transformData() {
    this.datasets[0].data = [];
    for (let i = 0; i < this.dataChart.length; i++) {
      let close;
      let open;
      let low;
      let high;
      if(this.dataChart[i - 1]) {
        open = this.datasets[0].data[i - 1].c;
      }
      else {
        open = 0;
      }
      if(this.dataChart[i].value > 0) {
        close = this.dataChart[i].value + open;
      }
      else {
        close = open + this.dataChart[i].value;
      }
      if(close > open) {
        low = open;
        high = close;
      }
      else {
        low = close;
        high = open;
      }
      this.datasets[0].data.push({"o": open, "c": close, "l": low, "h": high, "t": this.dataChart[i].timestamp})
    }
  }
}
