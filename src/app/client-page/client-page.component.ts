import { Component, OnInit } from '@angular/core';
import { FinancialServiceService } from '../financial-service.service';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-client-page',
  templateUrl: './client-page.component.html',
  styleUrls: ['./client-page.component.css']
})
export class ClientPageComponent implements OnInit {

  constructor(private financialService: FinancialServiceService, private router: Router, private formBuilder: FormBuilder) {}

  descriptionOperation;
  profitOperation;
  tagOperation;
  response;
  solde;
  win;
  lose;
  addModal = false;
  chartModal = false;
  dateModal = false;
  chartFinancial = [];
  dateStart;
  dateEnd;
  verifDate = false;
  addForm: FormGroup;

  public pieChartLabels:string[] = ['lose', 'profit'];
  public pieChartData:number[] = [this.lose, this.win];
  public pieChartType:string = 'pie';

  ngOnInit() {
    this.getAll();
    this.operationForm();
  }

  operationForm() {
    this.addForm = this.formBuilder.group({
      description: ['', [Validators.required, Validators.minLength(10)]],
      profit: ['', Validators.required],
      tag: ['', [Validators.required, Validators.minLength(3), Validators.maxLength(20)]]
    })
  }

  onSubmitForm() {
    let value = this.addForm.value;
    this.financialService.add(value).subscribe(
      data => {
          value.timestamp = (<any>data["date"]).timestamp * 1000; 
          value.date = new Date((<any>data["date"]).timestamp * 1000);
          value.id = data["id"]
          this.response.push(value);
          this.soldeWinLose(this.response);
          this.addModal = false;
      },
      error => {
          console.log("Error", error);
      }
    );
  }

  getAll() {
    this.financialService.getAll().subscribe(
      data => {this.response = data,
               this.soldeWinLose(data)
              },
      error => console.log("Error :: " + error),
    )
  }

  search(){
    let search = {"dateStart": this.dateStart, "dateEnd": this.dateEnd};
    this.financialService.search(search).subscribe(
      data => {this.response = data,
        this.soldeWinLose(data),
        this.verifDate = true
        this.dateModal = false
       },
       error => {console.log("Error :: " + error),
       this.router.navigate(['/', 'login']);},
    )
  }

  // add() {
  //   let operation = {"profit": this.profitOperation, "description": this.descriptionOperation, "tag": this.tagOperation, "timestamp": null, "date": null, "id": null};
  //   this.financialService.add(operation).subscribe(
  //     data => {
  //         operation.timestamp = (<any>data["date"]).timestamp * 1000; 
  //         operation.date = new Date((<any>data["date"]).timestamp * 1000);
  //         operation.id = data["id"]
  //         this.response.push(operation);
  //         this.soldeWinLose(this.response);
  //         this.addModal = false;
  //     },
  //     error => {
  //         console.log("Error", error);
  //     }
  // );
  // }

  remove(id) {
    this.financialService.remove(id).subscribe(data => {
      this.removeOperation(data);
    });
  }

  removeOperation(id) {
    for (let i = 0; i < this.response.length; i++) {
      if(this.response[i].id == id) {
        this.response.splice(i, 1);
        this.soldeWinLose(this.response);
      }
    }
  }

  Color(value) {
    if(value > 0) {
      return "green";
    }
    if(value < 0) {
      return "red";
    }
    if(value === 0) {
      return "black";
    }
  }

  soldeWinLose(data) {
    this.chartFinancial = [];
    this.solde = 0;
    this.win = 0;
    this.lose = 0;
    for (const item of data) {
      let number = +item.profit
      this.solde = number + this.solde
      if(number > 0) {
        this.win = number + this.win
      }
      if(number < 0) {
        this.lose = number + this.lose
      }
      this.chartFinancial.push({"value": number, "labels": item.tag, "date": item.date, "timestamp": item.timestamp})
    }
    this.win = this.win.toFixed(2);
    this.lose = this.lose.toFixed(2);
    this.solde = this.solde.toFixed(2);
    this.pieChartData = [this.lose, this.win];
  }
}
