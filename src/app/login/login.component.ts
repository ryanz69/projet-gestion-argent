import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  logForm: FormGroup

  constructor(private userService: UserService, private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.loginForm();
  }

  loginForm() {
    this.logForm = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required],
    })
  }

  onSubmitForm() {
    let value = this.logForm.value;
    this.userService.login(value).subscribe(
      error => {
            console.log(error)
    });
  }
}
