import { Injectable } from '@angular/core';
import { map } from "rxjs/operators";
import { HttpClient } from "@angular/common/http";

export interface RequestFinancial {
  description: string,
  date: Date,
  profit: number,
  timestamp: number,
}

@Injectable({
  providedIn: 'root'
})
export class FinancialServiceService {

  public result;

  constructor(private http: HttpClient) { }

  public getAll() {
    return this.http.get<RequestFinancial[]>('http://localhost:8080/api/operation/user')
      .pipe(map(operations => {
        for (let operation of operations) {
          operation.timestamp = (<any>operation.date).timestamp * 1000; 
          operation.date = new Date((<any>operation.date).timestamp * 1000);
        }
        return operations;
      }));
  }

  public search(search) {
    return this.http.post<RequestFinancial[]>('http://localhost:8080/api/operation/user/search', search)
    .pipe(map(operations => {
      for (let operation of operations) {
        operation.timestamp = (<any>operation.date).timestamp * 1000; 
        operation.date = new Date((<any>operation.date).timestamp * 1000);
      }
      return operations;
    }));;
  }

  public add(operation) {
    return this.http.post<RequestFinancial[]>('http://localhost:8080/api/operation/user/add', operation);
  }

  public remove(id) {
    return this.http.delete('http://localhost:8080/api/operation/user/remove/' + id)
  }
}
