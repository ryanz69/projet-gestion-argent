import { TestBed, inject } from '@angular/core/testing';

import { FinancialServiceService } from './financial-service.service';

describe('FinancialServiceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [FinancialServiceService]
    });
  });

  it('should be created', inject([FinancialServiceService], (service: FinancialServiceService) => {
    expect(service).toBeTruthy();
  }));
});
