import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { CookieService } from 'ngx-cookie-service';
import { UserService } from './user.service';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthInterceptorService implements HttpInterceptor {

  constructor(private cookieService: CookieService, private userService: UserService) { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    const idToken = this.cookieService.get('token');

    const cloned = req.clone({
        headers: req.headers.set("Authorization", idToken)
    });
    
    return next.handle(cloned).pipe(
      catchError(response => {
        if (response instanceof HttpErrorResponse) {
          if(response.status == 401) {
            this.userService.logout();
          }
        }
        return throwError(response);	 
      })
    );
  }
}

